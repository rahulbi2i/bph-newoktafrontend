import React from "react";
import {
  Routes,
  Router,
  Route,
  Redirect,
  Link,
  Switch,
} from "react-router-dom";
import PublicPage from "./PublicPage";
import Planning from "./Planning";
import { BrowserRouter } from "react-router-dom";
import SignInPage from "./SignInPage";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import URL from "./config";
import CategoryTrend from "./CategoryTrend";
import Innovation from "./Innovation";
import axios from "axios";

const AuthContext = React.createContext();

const App = () => {
  const history = useHistory();
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  function ProtectedRoute({ component: Component, ...restOfProps }) {
    return (
      <Route
        {...restOfProps}
        render={(props) =>
          isAuthenticated ? <Component {...props} /> : <Redirect to="/signin" />
        }
      />
    );
  }
  const Logout = () => {
    axios
      .get(`${URL}/logout`)
      .then((response) => response)
      .catch((error) => console.log(error));

    setIsAuthenticated(false);
    history.push("/signin");
  };
  return (
    <BrowserRouter>
      <AuthContext.Provider
        value={{
          isAuthenticated: isAuthenticated,
          setIsAuthenticated: setIsAuthenticated,
        }}
      >
        <button>
          <Link to="/home">HOME</Link>
        </button>
        <button>
          <Link to="/planning">PLANNING</Link>
        </button>
        <button>
          <Link to="/innovation">INNOVATION</Link>
        </button>
        <button>
          <Link to="/categorytrend">CATEGORY TREND</Link>
        </button>
        <button onClick={Logout}>LOGOUT</button>
        <Switch>
          <Route exact component={SignInPage} path="/signin" />
          <Route exact component={PublicPage} path="/home" />
          <ProtectedRoute exact component={Planning} path="/planning" />
          <ProtectedRoute exact component={Innovation} path="/innovation" />
          <ProtectedRoute
            exact
            component={CategoryTrend}
            path="/categorytrend"
          />
        </Switch>
      </AuthContext.Provider>
    </BrowserRouter>
  );
};

export default App;
export { AuthContext };
