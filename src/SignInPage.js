import React from "react";
import { AuthContext } from "./App";
import { useHistory } from "react-router-dom";
import URL from "./config";
import axios from "axios";

export default function () {
  const history = useHistory();
  const AuthValue = React.useContext(AuthContext);
  const Login = () => {
    AuthValue.setIsAuthenticated(true);
    history.push("/planning");
    // axios
    //   .get(`${URL}/auth_login`, { withCredentials: true })
    //   .then((response) => {
    //     AuthValue.setIsAuthenticated(true);
    //     history.push("/planning");
    //   })
    //   .catch((error) => console.log(error));
  };
  return (
    <div style={{ margin: "80px 0px 0px 80px" }}>
      <button onClick={Login}>CLICK TO MAKE TRUE</button>
      <button>
        {" "}
        <a href="http://localhost:8000/auth_login">CLICK TO SIGNIN Test</a>
      </button>
    </div>
  );
}
