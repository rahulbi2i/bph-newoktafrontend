FROM node:14.17.0

#Create app directory
# RUN mkdir -p /usr/src/app
WORKDIR /app
COPY package.json .

RUN npm install
RUN npm run build
COPY . .
EXPOSE 3000
CMD [ "npm", "start" ]